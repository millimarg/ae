
# A₂E -- Anno 1503 Explorer

<!--
This file is part of A₂E.
SPDX-FileCopyrightText: 2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
-->

A₂E is a tool for viewing, exploring, and modifying the contents of binary data
files of the 2002 game <a href="https://en.wikipedia.org/wiki/Anno_1503">Anno 1503</a>.</p>

It is a graphical frontend for [amsel](https://gitlab.com/millimarg/amsel), the
Anno Modding System and Exploration Library.

Head over to the [amsel](https://gitlab.com/millimarg/amsel) repository for more
information.


## Project status

**IN DEVELOPMENT**

The tool can be used to study Anno files but modification is not yet
implemented. Proper support for changing files and saving changes as mods that
can be published is on the roadmap.

Note that all features are already ready in [amsel](https://gitlab.com/millimarg/amsel).
You can build mods, apply them, mount Anno files as virtual filesystems, etc.
What is unfinished is only the GUI for easier access.


## How to get started

A₂E and Amsel run best in a Python virtual environment.

```bash
# Fetch the code
git clone https://gitlab.com/millimarg/ae
git submodule update --init --recursive --progress

pushd ae

# Prepare data folders
mkdir -p "$HOME/.local/share/amsel"
ln -s "$PWD/spec" "$HOME/.local/share/amsel/spec"
ln -s "$PWD/amsel/mod-templates" "$HOME/.local/share/amsel/mod-templates"

# Run the program (automatically sets up the environment)
./ae-py.sh -h
```

## Contributing

There are many ways you can contribute, and all contributions are welcome!

- use the library and build mods
- work on library documentation
- improve spec files and structure documentation
- build tools for studying and using Anno files
- ...and so much more!

**Need some easy tasks?** You can help a lot by finding hints at file structures
in the AnnoZone forums, and importing these bits of information into the spec
files! Always add a link to the original source of the information, of course.


## Acknowledgments

Thank you to Dickerbaer, Admiral Drake, DWOb, drkohler, and the
[AnnoZone](https://annozone.de/forum) community for your support and
insights!


## License

Copyright (C) 2024  Emily Margit Mueller (millimarg)

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <a href="https://www.gnu.org/licenses/">www.gnu.org/licenses</a>.
