#!/bin/bash
#
# This file is part of A₂E.
# SPDX-FileCopyrightText: 2024  Emily Margit Mueller
# SPDX-License-Identifier: GPL-3.0-or-later

# -------------------------------------------------------------------------- #

cENVNAME="ae-py"

function main() {
    if [[ "$1" == ".u" ]]; then
        shift
        pushd "$cBASEDIR/amsel"
        git pull
        popd
    fi

    export PYTHONPATH="$cBASEDIR/amsel:$PYTHONPATH"
    python3 -m amsel build-docs > "$cBASEDIR/ae/theme/specs.html"
    pyside6-project build "$cBASEDIR/ae/ae.pyproject" && python3 "$cBASEDIR/ae/main.py" "$@"
}

# -------------------------------------------------------------------------- #

cBASEDIR="$(dirname -- "$(realpath -- "$0")")"
cWORKDIR="$(pwd)"

pushd "$cBASEDIR" >/dev/null || {
    echo "error: cannot find base directory" >&2
    exit 1
}

cENVDIR="$cBASEDIR/env/$cENVNAME"

if [[ -d "$cENVDIR" ]]; then
    source -- "$cENVDIR/bin/activate"
else
    echo "initializing Python virtual environment..."

    mkdir -p "$cENVDIR"
    python3 -m venv "$cENVDIR"
    printf -- "%s\n" "$cENVNAME" > "$cBASEDIR/.venv"

    source -- "$cENVDIR/bin/activate"

    pip3 install --upgrade pip
    pip3 install -r "$cBASEDIR/requirements.txt"
fi

popd >/dev/null

main "$@"
