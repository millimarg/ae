# This file is part of A₂E.
# SPDX-FileCopyrightText: 2024  Emily Margit Mueller
# SPDX-License-Identifier: GPL-3.0-or-later

anytree==2.8.0
apng==0.3.4
beautifulsoup4==4.11.2
fusepy==3.0.1
Jinja2==3.1.2
jinja2-highlight==0.6.1
lxml==4.9.2
MarkupSafe==2.1.2
parsimonious==0.10.0
Pygments==2.14.0
pypng==0.20220715.0
PySide6==6.6.2
PySide6_Addons==6.6.2
PySide6_Essentials==6.6.2
pyxdg==0.28
regex==2022.10.31
shiboken6==6.6.2
six==1.16.0
soupsieve==2.3.2.post1
xmltodict==0.13.0
