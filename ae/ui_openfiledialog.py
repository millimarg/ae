# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'openfiledialog.ui'
##
## Created by: Qt User Interface Compiler version 6.6.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QAbstractButton, QApplication, QComboBox, QDialog,
    QDialogButtonBox, QFormLayout, QHBoxLayout, QLabel,
    QLineEdit, QSizePolicy, QToolButton, QVBoxLayout,
    QWidget)
import rc_resources

class Ui_OpenFileDialog(object):
    def setupUi(self, OpenFileDialog):
        if not OpenFileDialog.objectName():
            OpenFileDialog.setObjectName(u"OpenFileDialog")
        OpenFileDialog.setWindowModality(Qt.ApplicationModal)
        OpenFileDialog.resize(636, 241)
        icon = QIcon()
        icon.addFile(u":/theme/main_icon.ico", QSize(), QIcon.Normal, QIcon.Off)
        OpenFileDialog.setWindowIcon(icon)
        OpenFileDialog.setLocale(QLocale(QLocale.English, QLocale.UnitedStates))
        self.verticalLayout = QVBoxLayout(OpenFileDialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.formLayout_2 = QFormLayout()
        self.formLayout_2.setObjectName(u"formLayout_2")
        self.filePathLabel = QLabel(OpenFileDialog)
        self.filePathLabel.setObjectName(u"filePathLabel")

        self.formLayout_2.setWidget(0, QFormLayout.LabelRole, self.filePathLabel)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.filePath = QLineEdit(OpenFileDialog)
        self.filePath.setObjectName(u"filePath")

        self.horizontalLayout.addWidget(self.filePath)

        self.filePathButton = QToolButton(OpenFileDialog)
        self.filePathButton.setObjectName(u"filePathButton")
        icon1 = QIcon(QIcon.fromTheme(u"document-open"))
        self.filePathButton.setIcon(icon1)

        self.horizontalLayout.addWidget(self.filePathButton)


        self.formLayout_2.setLayout(0, QFormLayout.FieldRole, self.horizontalLayout)

        self.specLabel = QLabel(OpenFileDialog)
        self.specLabel.setObjectName(u"specLabel")

        self.formLayout_2.setWidget(1, QFormLayout.LabelRole, self.specLabel)

        self.specCombo = QComboBox(OpenFileDialog)
        self.specCombo.setObjectName(u"specCombo")
        self.specCombo.setEnabled(False)

        self.formLayout_2.setWidget(1, QFormLayout.FieldRole, self.specCombo)

        self.subsetPathLabel = QLabel(OpenFileDialog)
        self.subsetPathLabel.setObjectName(u"subsetPathLabel")

        self.formLayout_2.setWidget(2, QFormLayout.LabelRole, self.subsetPathLabel)

        self.horizontalLayout_5 = QHBoxLayout()
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.subsetPath = QLineEdit(OpenFileDialog)
        self.subsetPath.setObjectName(u"subsetPath")
        self.subsetPath.setEnabled(False)

        self.horizontalLayout_5.addWidget(self.subsetPath)

        self.subsetPathButton = QToolButton(OpenFileDialog)
        self.subsetPathButton.setObjectName(u"subsetPathButton")
        self.subsetPathButton.setEnabled(False)
        icon2 = QIcon(QIcon.fromTheme(u"edit-select"))
        self.subsetPathButton.setIcon(icon2)

        self.horizontalLayout_5.addWidget(self.subsetPathButton)


        self.formLayout_2.setLayout(2, QFormLayout.FieldRole, self.horizontalLayout_5)

        self.subsetPathHelp = QLabel(OpenFileDialog)
        self.subsetPathHelp.setObjectName(u"subsetPathHelp")
        self.subsetPathHelp.setEnabled(False)
        self.subsetPathHelp.setTextFormat(Qt.PlainText)
        self.subsetPathHelp.setWordWrap(True)

        self.formLayout_2.setWidget(3, QFormLayout.FieldRole, self.subsetPathHelp)


        self.verticalLayout.addLayout(self.formLayout_2)

        self.buttonBox = QDialogButtonBox(OpenFileDialog)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Open)

        self.verticalLayout.addWidget(self.buttonBox)


        self.retranslateUi(OpenFileDialog)
        self.buttonBox.accepted.connect(OpenFileDialog.accept)
        self.buttonBox.rejected.connect(OpenFileDialog.reject)

        QMetaObject.connectSlotsByName(OpenFileDialog)
    # setupUi

    def retranslateUi(self, OpenFileDialog):
        OpenFileDialog.setWindowTitle(QCoreApplication.translate("OpenFileDialog", u"Load a file", None))
        self.filePathLabel.setText(QCoreApplication.translate("OpenFileDialog", u"File path:", None))
        self.filePathButton.setText(QCoreApplication.translate("OpenFileDialog", u"Select a file", None))
        self.specLabel.setText(QCoreApplication.translate("OpenFileDialog", u"File format:", None))
        self.subsetPathLabel.setText(QCoreApplication.translate("OpenFileDialog", u"Subset path:", None))
        self.subsetPath.setPlaceholderText(QCoreApplication.translate("OpenFileDialog", u"ANNO/ANLAGEN/ENTRY", None))
        self.subsetPathButton.setText(QCoreApplication.translate("OpenFileDialog", u"Select a path from the file type specification", None))
        self.subsetPathHelp.setText(QCoreApplication.translate("OpenFileDialog", u"If the file contains only part of a binary file, you must select the file format manually, and specify the spec path of the extract. For example, if you want to load a file containing a single Anlagen entry, select \u201cAnlagen\u201d as file format and pick \u201cANNO/ANLAGEN/ENTRY\u201d as subset path.", None))
    # retranslateUi

