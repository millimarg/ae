# kate: syntax python; space-indent on; indent-width 4;

'''
This file is part of A₂E.
SPDX-FileCopyrightText: 2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
'''

import sys
import argparse
import textwrap

from PySide6.QtWidgets import QApplication
from PySide6.QtWidgets import QErrorMessage

from mainwindow import MainWindow


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description='A₂E — Anno 1503 Explorer',
        epilog=textwrap.dedent('''\
            --------------------------------------------------------------------

            Copyright (C) 2024  Emily Margit Mueller (millimarg)

            This program is free software: you can redistribute it and/or modify
            it under the terms of the GNU General Public License as published by
            the Free Software Foundation, either version 3 of the License, or (at
            your option) any later version.

            This program is distributed in the hope that it will be useful,
            but WITHOUT ANY WARRANTY; without even the implied warranty of
            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
            General Public License for more details.

            You should have received a copy of the GNU General Public
            License along with this program. If not, see www.gnu.org/licenses .
        '''))
    parser.add_argument('infile', type=str, help='binary input file to load',
                        default='', nargs='?')

    args = parser.parse_args()

    app = QApplication(sys.argv)
    QErrorMessage.qtHandler()

    window = MainWindow(args.infile)
    window.show()

    sys.exit(app.exec())
