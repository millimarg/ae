# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'specdocdialog.ui'
##
## Created by: Qt User Interface Compiler version 6.6.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWebEngineWidgets import QWebEngineView
from PySide6.QtWidgets import (QAbstractButton, QApplication, QDialog, QDialogButtonBox,
    QSizePolicy, QVBoxLayout, QWidget)
import rc_resources

class Ui_SpecDocDialog(object):
    def setupUi(self, SpecDocDialog):
        if not SpecDocDialog.objectName():
            SpecDocDialog.setObjectName(u"SpecDocDialog")
        SpecDocDialog.resize(1250, 800)
        icon = QIcon()
        icon.addFile(u":/theme/main_icon.ico", QSize(), QIcon.Normal, QIcon.Off)
        SpecDocDialog.setWindowIcon(icon)
        SpecDocDialog.setLocale(QLocale(QLocale.English, QLocale.UnitedStates))
        self.verticalLayout = QVBoxLayout(SpecDocDialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.webview = QWebEngineView(SpecDocDialog)
        self.webview.setObjectName(u"webview")
        sizePolicy = QSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.webview.sizePolicy().hasHeightForWidth())
        self.webview.setSizePolicy(sizePolicy)
        self.webview.setUrl(QUrl(u"qrc:/theme/specs.html"))

        self.verticalLayout.addWidget(self.webview)

        self.buttonBox = QDialogButtonBox(SpecDocDialog)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Close)

        self.verticalLayout.addWidget(self.buttonBox)


        self.retranslateUi(SpecDocDialog)
        self.buttonBox.rejected.connect(SpecDocDialog.reject)
        self.buttonBox.accepted.connect(SpecDocDialog.accept)

        QMetaObject.connectSlotsByName(SpecDocDialog)
    # setupUi

    def retranslateUi(self, SpecDocDialog):
        SpecDocDialog.setWindowTitle(QCoreApplication.translate("SpecDocDialog", u"File Format Specifications", None))
    # retranslateUi

