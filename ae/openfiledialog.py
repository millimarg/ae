# kate: syntax python; space-indent on; indent-width 4;

'''
This file is part of A₂E.
SPDX-FileCopyrightText: 2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
'''

from pathlib import Path

from PySide6.QtWidgets import QDialog
from PySide6.QtWidgets import QFileDialog
from PySide6.QtCore import Property
from PySide6.QtCore import Signal

from amsel.spec import Spec

from tools import getSettings
from ui_openfiledialog import Ui_OpenFileDialog


class OpenFileDialog(QDialog):
    _dialogOpened = Signal()
    filePathChanged = Signal()
    specChanged = Signal()
    specPathChanged = Signal()

    def __init__(self, parent):
        super(OpenFileDialog, self).__init__(parent)
        self.ui = Ui_OpenFileDialog()
        self.ui.setupUi(self)

        self.ui.filePathButton.clicked.connect(self._pickFilePath)
        self.ui.subsetPathButton.clicked.connect(self._pickSpecPath)
        self._populateSpecList()

        self._p_filePath: str = ''
        self._p_spec: str = ''
        self._p_specPath: str = ''

        # connections
        self._dialogOpened.connect(self._handleDialogOpened)
        self.ui.filePath.textChanged.connect(self._setNewFile)
        self.ui.specCombo.currentIndexChanged.connect(self._setNewSpec)

        # disable unimplemented features
        self.ui.subsetPathButton.setEnabled(False)
        self.ui.subsetPathLabel.setEnabled(False)

    def open(self):
        super(OpenFileDialog, self).open()
        self._dialogOpened.emit()

    def _handleDialogOpened(self):
        # start picking a path automatically to save a click
        self.hide()

        settings = getSettings()
        initial = settings.value('openFile/lastFile')
        initial = Path(initial).parent if initial else ''
        self._pickFilePath(initial)

        if not self._p_filePath:
            self.reject()
        else:
            self.show()

    def _populateSpecList(self):
        self.ui.specCombo.setEnabled(False)
        self.ui.specLabel.setEnabled(False)
        self.ui.specCombo.clear()

        sorted_defs = [v for k, v in sorted(Spec.get_definitions().items(), key=lambda x: x[1].title)]

        for i in reversed(sorted_defs):
            self.ui.specCombo.insertItem(-1, i.title, i.ident)

        self.ui.specCombo.setCurrentIndex(-1)

        if sorted_defs:
            self.ui.specCombo.setEnabled(True)
            self.ui.specLabel.setEnabled(True)

    def _pickFilePath(self, initial: str = ''):
        # set the variable directly to avoid triggering the filePathChanged()
        # signal with its handlers
        new_path = QFileDialog.getOpenFileName(
            self, self.tr('Select a file'), str(initial) or self._p_filePath,
            self.tr('Anno files (*.dat *.tex *.inc *.zei *.sav *.sze *.cach *.txt *.scp *.addon *.classic *.dll)'))[0]

        if new_path:
            self._p_filePath = new_path
            self.ui.filePath.setText(self._p_filePath)

    def _pickSpecPath(self):
        pass

    def _detectFileFormat(self):
        if not self._p_filePath or not Path(self._p_filePath).is_file():
            self.ui.specCombo.setCurrentIndex(-1)
            return

        try:
            spec = Spec.by_magic(self._p_filePath)
        except FileNotFoundError:
            spec = None

        if spec:
            self.ui.specCombo.setCurrentIndex(
                self.ui.specCombo.findData(spec.ident))
        else:
            self.ui.specCombo.setCurrentIndex(-1)

    def _setNewSpec(self):
        self.spec = self.ui.specCombo.currentData() or ''

    def _setNewFile(self):
        self.filePath = self.ui.filePath.text()
        self._detectFileFormat()

        if self._p_filePath and Path(self._p_filePath).is_file():
            settings = getSettings()
            settings.setValue('openFile/lastFile', self._p_filePath)

    @Property(str, notify=filePathChanged)
    def filePath(self) -> str:
        return self._p_filePath

    @filePath.setter
    def filePath(self, v) -> None:
        if self._p_filePath != v:
            self._p_filePath = v
            self.filePathChanged.emit()

    @Property(str, notify=specChanged)
    def spec(self) -> str:
        return self._p_spec

    @spec.setter
    def spec(self, v) -> None:
        if self._p_spec != v:
            self._p_spec = v
            self.specChanged.emit()

    @Property(str, notify=specPathChanged)
    def specPath(self) -> str:
        return self._p_specPath

    @specPath.setter
    def specPath(self, v) -> None:
        if self._p_specPath != v:
            self._p_specPath = v
            self.specPathChanged.emit()
