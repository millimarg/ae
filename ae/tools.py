# kate: syntax python; space-indent on; indent-width 4;

'''
This file is part of A₂E.
SPDX-FileCopyrightText: 2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
'''

from PySide6.QtCore import QSettings


def getSettings() -> QSettings:
    return QSettings('millimarg', 'ae')
