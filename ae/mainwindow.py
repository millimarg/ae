# kate: syntax python; space-indent on; indent-width 4;

'''
This file is part of A₂E.
SPDX-FileCopyrightText: 2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
'''

import textwrap
import enum
from pathlib import Path
from typing import List

from PySide6.QtWidgets import QMainWindow
from PySide6.QtWidgets import QDialog
from PySide6.QtWidgets import QFileDialog
from PySide6.QtWidgets import QProgressDialog
from PySide6.QtWidgets import QTreeWidgetItem
from PySide6.QtWidgets import QMessageBox
from PySide6.QtWidgets import QPlainTextEdit
from PySide6.QtWidgets import QVBoxLayout
# from PySide6.QtWidgets import QLineEdit
from PySide6.QtWidgets import QWidget
# from PySide6.QtGui import QValidator
from PySide6.QtGui import QPalette
from PySide6.QtCore import QObject
from PySide6.QtCore import QSortFilterProxyModel
from PySide6.QtCore import QModelIndex
from PySide6.QtCore import Property
from PySide6.QtCore import Signal
from PySide6.QtCore import Slot
from PySide6.QtCore import Qt

from amsel.spec import Spec
from amsel.path import Path as SpecPath
from amsel.path import PathError as SpecPathError
from amsel.path import InvalidPathError
from amsel.parser import Parser
from amsel.parser import ParsedNode
from amsel.docs import formatSpecDocs

from tools import getSettings
from openfiledialog import OpenFileDialog
from docdialogs import SpecDocDialog
from ui_mainwindow import Ui_MainWindow


class ModelRoles(enum.IntEnum):
    PARSED_NODE_ROLE = Qt.UserRole + 1


# class PathValidator(QValidator):
#     def __init__(self, parent: QObject = None):
#         super(PathValidator, self).__init__(parent)
#
#     def validate(self, text: str, pos: int) -> QValidator.State:
#         if not text:
#             return QValidator.Acceptable
#
#         try:
#             SpecPath(text)
#         except (SpecPathError, InvalidPathError):
#             return QValidator.Intermediate
#
#         return QValidator.Acceptable


class PathFilterModel(QSortFilterProxyModel):
    def __init__(self, parent: QObject = None):
        super(PathFilterModel, self).__init__(parent)
        self.setRecursiveFilteringEnabled(True)

    @staticmethod
    def isValidQuery(query: str) -> bool:
        if not query:
            return True

        try:
            SpecPath(query)
        except (SpecPathError, InvalidPathError):
            return False

        return True

    def filterAcceptsRow(self, sourceRow: int, sourceParent: QModelIndex) -> bool:
        query = self.filterRegularExpression().pattern()

        if not query:
            return True

        query = SpecPath(query)
        index = self.sourceModel().index(sourceRow, 0, sourceParent)

        if not index:
            return False

        node = self.sourceModel().data(index, ModelRoles.PARSED_NODE_ROLE)

        if not node:
            return False

        return query.match(node)


class MainWindow(QMainWindow):
    # ------ Signals

    openFileNameChanged = Signal()
    openFilePathChanged = Signal()
    openFileSpecChanged = Signal()
    openFileSubsetChanged = Signal()

    # ------ Methods

    def __init__(self, initialFile: str = None):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.ui.splitterLeftRight.setSizes([150, 250])
        self.ui.splitterTopBottom.setSizes([250, 250])
        self.loadSettings()

        # properties
        self._parser: Parser = None
        self._allTreeItems: List[QTreeWidgetItem] = []

        self._p_openFileName: str = ''
        self._p_openFilePath: Path = None
        self._p_openFileSpec: str = ''
        self._p_openFileSubset: str = ''

        self._proxy: PathFilterModel = PathFilterModel(self)
        self._proxy.setSourceModel(self.ui.treeWidget.model())
        self.ui.treeView.setModel(self._proxy)
        self.ui.treeWidget.hide()
        self.ui.treeView.setColumnWidth(0, 300)
        self.ui.treeView.setColumnWidth(1, 100)
        self.ui.treeView.setColumnWidth(2, 100)
        self.ui.treeView.setColumnWidth(4, 75)
        self.ui.treeView.setColumnWidth(5, 75)
        # self.ui.filterLine.setValidator(PathValidator(self))
        self._defaultTextColor = self.ui.filterLine.palette().color(QPalette.Text)

        # connections
        self.ui.actionOpen.triggered.connect(self._openFile)
        self.ui.actionQuickOpen.triggered.connect(self._quickOpenFile)
        self.ui.actionAbout.triggered.connect(self._showAbout)
        self.ui.actionAboutQt.triggered.connect(lambda: QMessageBox.aboutQt(self))
        self.ui.buttonExpandAll.clicked.connect(self.ui.treeView.expandAll)
        self.ui.buttonCollapseAll.clicked.connect(self.ui.treeView.collapseAll)
        self.ui.treeView.selectionModel().currentChanged.connect(self._updateDetailsPanel)
        self.ui.filterButton.clicked.connect(self._applyFilter)
        self.ui.filterLine.textChanged.connect(self._checkFilter)
        self.ui.actionSpecs.triggered.connect(self._showSpecDocs)
        self.openFilePathChanged.connect(self._updateTitleAndButtons)
        self.openFilePathChanged.connect(lambda: self.ui.currentFileName.setText(self._p_openFilePath))

        self.setWindowModified(False)
        self._updateTitleAndButtons()

        if initialFile:
            self._doOpenPath(initialFile)

    def closeEvent(self, event):
        if self._parser:
            self._parser.close()

        settings = getSettings()
        settings.setValue('mainWindow/geometry', self.saveGeometry())
        settings.setValue('mainWindow/windowState', self.saveState())
        settings.setValue('mainWindow/splitterLeftRightState', self.ui.splitterLeftRight.saveState())
        settings.setValue('mainWindow/splitterTopBottomState', self.ui.splitterTopBottom.saveState())

        super(MainWindow, self).closeEvent(event)

    def loadSettings(self):
        settings = getSettings()
        self.restoreGeometry(settings.value('mainWindow/geometry'))
        self.restoreState(settings.value('mainWindow/windowState'))
        self.ui.splitterLeftRight.restoreState(settings.value('mainWindow/splitterLeftRightState'))
        self.ui.splitterTopBottom.restoreState(settings.value('mainWindow/splitterTopBottomState'))

    def _makeEditorWidget(self, node: ParsedNode):
        if not node.specNode.isData:
            return

        # if node.specNode.typeStr == 'uint':
        #     line = QLineEdit(self.ui.contentsView)
        #     self.ui.contentsView.layout().addWidget(line)

        line = QPlainTextEdit(node.decodeToStr(), self.ui.contentsView)
        self.ui.contentsView.layout().addWidget(line)

    # ------ Slots

    @Slot()
    def _warning(self, text, title: str = None):
        if not title:
            title = self.tr('Warning')

        QMessageBox.warning(self, title, text)

    @Slot()
    def _checkFilter(self) -> bool:
        query = self.ui.filterLine.text()
        palette = self.ui.filterLine.palette()

        if not PathFilterModel.isValidQuery(query):
            palette.setColor(QPalette.Text, Qt.red)
            self.ui.filterLine.setPalette(palette)
            return False
        else:
            palette.setColor(QPalette.Text, self._defaultTextColor)
            self.ui.filterLine.setPalette(palette)

        return True

    @Slot()
    def _applyFilter(self):
        if not self._checkFilter():
            return

        self._proxy.setFilterRegularExpression(self.ui.filterLine.text())

    @Slot()
    def _showSpecDocs(self):
        doc = SpecDocDialog(self)
        doc.show()

    @Slot()
    def _currentSelectionChanged(self, current: QModelIndex, previous: QModelIndex):
        self._updateDetailsPanel(self._proxy.mapToSource(current))

    @Slot()
    def _updateDetailsPanel(self, index: QModelIndex):
        if not index or not index.isValid():
            self.ui.currentBlockTitle.setText('')
            self.ui.currentBlockType.setText('')
            self.ui.currentBlockId.setText('')
            self.ui.currentBlockLength.setText('')
            self.ui.currentBlockOffset.setText('')
            self.ui.currentBlockLocalOffset.setText('')
            self.ui.docsView.setText('')

            for i in self.ui.contentsView.findChildren(QWidget):
                i.deleteLater()

            return

        def item(column):
            return index.sibling(index.row(), column)

        node = item(0).data(ModelRoles.PARSED_NODE_ROLE)

        if not node:
            print("error: no spec node for", index)
            return

        spec = node.specNode
        print("showing details for:", node)

        self.ui.currentBlockTitle.setText(node.name)
        self.ui.currentBlockType.setText(spec.typeStr)
        self.ui.currentBlockId.setText(spec.ident or item(1).data(Qt.DisplayRole))
        self.ui.currentBlockLength.setText(f'{node.fullLength}')
        self.ui.currentBlockOffset.setText(f'{node.globalStartOffset}')
        self.ui.currentBlockLocalOffset.setText(f'{node.localStartOffset}')

        what = formatSpecDocs(spec.docWhat)
        details = formatSpecDocs(spec.docDetails)

        title = f'#{spec.ident}' if spec.ident else node.name

        self.ui.docsView.setText(f'''
            <h3>{title}</h3>
            <h4>Path: {node.specPathWithIndex}</h4>
            <p>{what}</p>
            <p>{details}</p>
        ''')

        self.ui.contentsView.deleteLater()
        self.ui.contentsView = QWidget()
        self.ui.contentsView.setLayout(QVBoxLayout())
        self.ui.contentsViewScrollArea.setWidget(self.ui.contentsView)
        self._makeEditorWidget(node)
        self.ui.contentsView.layout().addStretch(1)

    @Slot()
    def _showAbout(self):
        QMessageBox.about(self, self.tr('About A₂E'), self.tr(textwrap.dedent('''
            <h2>About A₂E</h2>

            <p>A₂E – the Anno 1503 Explorer – is a tool for viewing and
            modifying the contents of binary data files of the 2002 game <a
            href="https://en.wikipedia.org/wiki/Anno_1503">Anno 1503</a>.</p>

            <p>This program is based on the <a href="https://gitlab.com/millimarg/amsel">amsel</a> library.</p>

            <h3>Acknowledgments</h3>

            <p>Thank you to Dickerbaer, Admiral Drake, DWOb, drkohler, and the
            <a href="https://annozone.de/forum">AnnoZone</a> community for your
            support and insights.</p>

            <h3>License</h3>

            <p><tt>Copyright (C) 2024<br>&nbsp;&nbsp;&nbsp;&nbsp;
            Emily Margit Mueller (millimarg)</tt></p>

            <p><tt>This program is free software: you can redistribute it and/or
            modify it under the terms of the GNU General Public License as published by the
            Free Software Foundation, either version 3 of the License, or (at your option)
            any later version.</tt></p>

            <p><tt>This program is distributed in the hope that it will be useful,
            but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
            or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
            details.</tt></p>

            <p><tt>You should have received a copy of the GNU General Public
            License along with this program. If not, see
            <a href="https://www.gnu.org/licenses/">www.gnu.org/licenses</a>.</tt></p>
        ''')))

    @Slot()
    def _openFile(self):
        dialog = OpenFileDialog(self)
        dialog.finished.connect(lambda: self._handleOpenFile(dialog))
        dialog.open()

    @Slot()
    def _quickOpenFile(self):
        settings = getSettings()
        initial = settings.value('openFile/lastFile')
        initial = Path(initial).parent if initial else ''

        path = QFileDialog.getOpenFileName(
            self, self.tr('Select a file'), str(initial) or self.openFilePath,
            self.tr('Anno files (*.dat *.tex *.inc *.zei *.sav *.sze *.cach *.txt *.scp *.addon *.classic *.dll)'))[0]

        self._doOpenPath(path)

    @Slot()
    def _doOpenPath(self, path):
        if not path:
            return

        if not Path(path).is_file():
            self._warning(self.tr('<tt>{f}</tt> is not a file and cannot be opened.').format(f=path))
            return

        try:
            spec = Spec.by_magic(path)
        except FileNotFoundError:
            spec = None

        if not spec:
            self._warning(self.tr('The file format of <tt>{f}</tt> cannot be recognized. '
                                  'If this is an extract from a valid Anno 1503 data file, '
                                  'specify the object path in the File Open dialog.').
                          format(f=path))
            return

        self.openFileName = Path(path).name
        self.openFilePath = path
        self.openFileSpec = spec.ident
        self.openFileSubset = ''
        self._parseOpenFile()

    @Slot()
    def _handleOpenFile(self, dialog):
        if dialog.result() == QDialog.Accepted:
            self.openFileName = Path(dialog.filePath).name
            self.openFilePath = dialog.filePath
            self.openFileSpec = dialog.spec
            self.openFileSubset = dialog.specPath

            self._parseOpenFile()

    @Slot()
    def _parseOpenFile(self):
        # Parse data

        self.statusBar().showMessage(self.tr('Parsing {file}... This may take a while.').format(file=self._p_openFileName), 0)
        progress1 = QProgressDialog(self.tr('Parsing...'), None, 0, 100, self)
        progress1.setWindowModality(Qt.WindowModal)
        progress1.setMinimumDuration(0)

        self._parser = Parser(dataFile=self._p_openFilePath, structure=self._p_openFileSpec,
                              part=self._p_openFileSubset)

        try:
            self._parser.parseFully(progressCallback=lambda x: progress1.setValue(x))
        except Exception as e:
            self._warning(self.tr('The following error occurred while parsing the '
                                  'file <tt>{f}</tt>:<br><br><tt>{e}</tt>').
                          format(f=self.openFilePath, e=str(e)), self.tr('Failed to parse'))

        progress1.setValue(100)
        self.statusBar().showMessage(self.tr('Parsing finished.'), 4000)

        # Load tree view

        self.statusBar().showMessage(self.tr('Loading node tree... This may take a while.'), 0)
        progress2 = QProgressDialog(self.tr('Loading...'), None, 0, 100, self)
        progress2.setWindowModality(Qt.WindowModal)
        progress2.setMinimumDuration(0)
        progress2.setValue(1)

        self.ui.treeWidget.clear()

        def count_descendants(parent) -> int:
            if parent.children:
                return len(parent.children) + sum([count_descendants(x) for x in parent.children])
            return 0

        descendants = count_descendants(self._parser.parsedRoot)
        progress2.setMaximum(descendants + 1)

        def build_tree(parent) -> QTreeWidgetItem:
            item = QTreeWidgetItem([parent.name, parent.specNode.ident, parent.specNode.typeStr,
                                    f'{parent.fullLength}', f'{parent.globalStartOffset}'])
            item.setData(0, ModelRoles.PARSED_NODE_ROLE, parent)
            progress2.setValue(progress2.value() + 1)

            if parent.entryIdNode:
                if parent.specNode.ident:
                    item.setText(1, f'{parent.specNode.ident} ({parent.entryIdNode.specNode.ident}: {parent.entryIdNode.decodeToStr()})')
                else:
                    item.setText(1, f'{parent.entryIdNode.decodeToStr()} ({parent.entryIdNode.specNode.ident})')

            for i in parent.children:
                item.addChild(build_tree(i))

            return item

        self.ui.treeWidget.addTopLevelItem(build_tree(self._parser.parsedRoot))

        progress2.setValue(progress2.maximum())
        self.statusBar().showMessage(self.tr('Loaded {count} entries.', '', descendants).
                                     format(count=descendants), 4000)

    @Slot()
    def _updateTitleAndButtons(self):
        if self._p_openFileName:
            self.setWindowTitle(self.tr('A₂E — {fileName} [*]').format(fileName=self.openFileName))
        elif self._p_openFilePath is not None:
            self.setWindowTitle(self.tr('A₂E — Unnamed [*]'))
        else:
            self.setWindowTitle(self.tr('A₂E — Anno 1503 Explorer [*]'))

        if self._p_openFileName or self._p_openFilePath:
            self.ui.buttonExpandAll.setEnabled(True)
            self.ui.buttonCollapseAll.setEnabled(True)
        else:
            self.ui.buttonExpandAll.setEnabled(False)
            self.ui.buttonCollapseAll.setEnabled(False)

    # ------ Properties

    @Property(str, notify=openFileNameChanged)
    def openFileName(self) -> str:
        return self._p_openFileName

    @openFileName.setter
    def openFileName(self, v) -> None:
        if self._p_openFileName != v:
            self._p_openFileName = v
            self.openFileNameChanged.emit()

    @Property(Path, notify=openFilePathChanged)
    def openFilePath(self) -> Path:
        return self._p_openFilePath

    @openFilePath.setter
    def openFilePath(self, v) -> None:
        if self._p_openFilePath != v:
            self._p_openFilePath = v
            self.openFilePathChanged.emit()

    @Property(str, notify=openFileSpecChanged)
    def openFileSpec(self) -> str:
        return self._p_openFileSpec

    @openFileSpec.setter
    def openFileSpec(self, v) -> None:
        if self._p_openFileSpec != v:
            self._p_openFileSpec = v
            self.openFileSpecChanged.emit()

    @Property(str, notify=openFileSubsetChanged)
    def openFileSubset(self) -> str:
        return self._p_openFileSubset

    @openFileSubset.setter
    def openFileSubset(self, v) -> None:
        if self._p_openFileSubset != v:
            self._p_openFileSubset = v
            self.openFileSubsetChanged.emit()
