# kate: syntax python; space-indent on; indent-width 4;

'''
This file is part of A₂E.
SPDX-FileCopyrightText: 2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
'''

from PySide6.QtWidgets import QDialog

# from tools import getSettings
from ui_specdocdialog import Ui_SpecDocDialog


class SpecDocDialog(QDialog):
    def __init__(self, parent):
        super(SpecDocDialog, self).__init__(parent)
        self.ui = Ui_SpecDocDialog()
        self.ui.setupUi(self)
        # self.loadSettings()

    # def closeEvent(self, event):
    #     settings = getSettings()
    #     settings.setValue('specDocDialog/geometry', self.saveGeometry())
    #     settings.setValue('specDocDialog/windowState', self.saveState())
    #     super(SpecDocDialog, self).closeEvent(event)
    #
    # def loadSettings(self):
    #     settings = getSettings()
    #     self.restoreGeometry(settings.value('specDocDialog/geometry'))
    #     self.restoreState(settings.value('specDocDialog/windowState'))
