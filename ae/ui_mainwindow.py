# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'mainwindow.ui'
##
## Created by: Qt User Interface Compiler version 6.6.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QAction, QBrush, QColor, QConicalGradient,
    QCursor, QFont, QFontDatabase, QGradient,
    QIcon, QImage, QKeySequence, QLinearGradient,
    QPainter, QPalette, QPixmap, QRadialGradient,
    QTransform)
from PySide6.QtWidgets import (QApplication, QFrame, QGridLayout, QHBoxLayout,
    QHeaderView, QLabel, QLineEdit, QMainWindow,
    QMenu, QMenuBar, QPushButton, QScrollArea,
    QSizePolicy, QSplitter, QStatusBar, QTextBrowser,
    QToolButton, QTreeView, QTreeWidget, QTreeWidgetItem,
    QVBoxLayout, QWidget)
import rc_resources

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(800, 600)
        MainWindow.setMinimumSize(QSize(800, 600))
        icon = QIcon()
        icon.addFile(u":/theme/main_icon.ico", QSize(), QIcon.Normal, QIcon.Off)
        MainWindow.setWindowIcon(icon)
        MainWindow.setLocale(QLocale(QLocale.English, QLocale.UnitedStates))
        self.actionDocs = QAction(MainWindow)
        self.actionDocs.setObjectName(u"actionDocs")
        self.actionDocs.setEnabled(False)
        icon1 = QIcon(QIcon.fromTheme(u"documentation"))
        self.actionDocs.setIcon(icon1)
        self.actionSpecs = QAction(MainWindow)
        self.actionSpecs.setObjectName(u"actionSpecs")
        self.actionSpecs.setEnabled(True)
        icon2 = QIcon(QIcon.fromTheme(u"structure"))
        self.actionSpecs.setIcon(icon2)
        self.actionAbout = QAction(MainWindow)
        self.actionAbout.setObjectName(u"actionAbout")
        self.actionAbout.setEnabled(True)
        icon3 = QIcon(QIcon.fromTheme(u"help-about"))
        self.actionAbout.setIcon(icon3)
        self.actionQuickOpen = QAction(MainWindow)
        self.actionQuickOpen.setObjectName(u"actionQuickOpen")
        icon4 = QIcon(QIcon.fromTheme(u"document-open"))
        self.actionQuickOpen.setIcon(icon4)
        self.actionQuit = QAction(MainWindow)
        self.actionQuit.setObjectName(u"actionQuit")
        icon5 = QIcon(QIcon.fromTheme(u"application-exit"))
        self.actionQuit.setIcon(icon5)
        self.actionSaveAs = QAction(MainWindow)
        self.actionSaveAs.setObjectName(u"actionSaveAs")
        self.actionSaveAs.setEnabled(False)
        icon6 = QIcon(QIcon.fromTheme(u"document-save-as"))
        self.actionSaveAs.setIcon(icon6)
        self.actionSave = QAction(MainWindow)
        self.actionSave.setObjectName(u"actionSave")
        self.actionSave.setEnabled(False)
        icon7 = QIcon(QIcon.fromTheme(u"document-save"))
        self.actionSave.setIcon(icon7)
        self.actionExportXML = QAction(MainWindow)
        self.actionExportXML.setObjectName(u"actionExportXML")
        self.actionExportXML.setEnabled(False)
        icon8 = QIcon(QIcon.fromTheme(u"document-export"))
        self.actionExportXML.setIcon(icon8)
        self.actionNew = QAction(MainWindow)
        self.actionNew.setObjectName(u"actionNew")
        self.actionNew.setEnabled(False)
        icon9 = QIcon(QIcon.fromTheme(u"document-new"))
        self.actionNew.setIcon(icon9)
        self.actionExportMod = QAction(MainWindow)
        self.actionExportMod.setObjectName(u"actionExportMod")
        self.actionExportMod.setEnabled(False)
        icon10 = QIcon(QIcon.fromTheme(u"package"))
        self.actionExportMod.setIcon(icon10)
        self.actionOpen = QAction(MainWindow)
        self.actionOpen.setObjectName(u"actionOpen")
        self.actionOpen.setIcon(icon4)
        self.actionAboutQt = QAction(MainWindow)
        self.actionAboutQt.setObjectName(u"actionAboutQt")
        icon11 = QIcon(QIcon.fromTheme(u"qtlogo"))
        self.actionAboutQt.setIcon(icon11)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.verticalLayout_3 = QVBoxLayout(self.centralwidget)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.x_currentFileLabel = QLabel(self.centralwidget)
        self.x_currentFileLabel.setObjectName(u"x_currentFileLabel")
        sizePolicy = QSizePolicy(QSizePolicy.Policy.Preferred, QSizePolicy.Policy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.x_currentFileLabel.sizePolicy().hasHeightForWidth())
        self.x_currentFileLabel.setSizePolicy(sizePolicy)
        font = QFont()
        font.setBold(True)
        self.x_currentFileLabel.setFont(font)

        self.horizontalLayout_3.addWidget(self.x_currentFileLabel)

        self.currentFileName = QLabel(self.centralwidget)
        self.currentFileName.setObjectName(u"currentFileName")
        sizePolicy1 = QSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Minimum)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.currentFileName.sizePolicy().hasHeightForWidth())
        self.currentFileName.setSizePolicy(sizePolicy1)

        self.horizontalLayout_3.addWidget(self.currentFileName)


        self.verticalLayout_3.addLayout(self.horizontalLayout_3)

        self.splitterLeftRight = QSplitter(self.centralwidget)
        self.splitterLeftRight.setObjectName(u"splitterLeftRight")
        sizePolicy2 = QSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Expanding)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.splitterLeftRight.sizePolicy().hasHeightForWidth())
        self.splitterLeftRight.setSizePolicy(sizePolicy2)
        self.splitterLeftRight.setOrientation(Qt.Horizontal)
        self.splitterLeftRight.setChildrenCollapsible(False)
        self.verticalLayoutWidget = QWidget(self.splitterLeftRight)
        self.verticalLayoutWidget.setObjectName(u"verticalLayoutWidget")
        self.verticalLayout = QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.treeWidget = QTreeWidget(self.verticalLayoutWidget)
        self.treeWidget.setObjectName(u"treeWidget")
        self.treeWidget.setContextMenuPolicy(Qt.CustomContextMenu)

        self.verticalLayout.addWidget(self.treeWidget)

        self.treeView = QTreeView(self.verticalLayoutWidget)
        self.treeView.setObjectName(u"treeView")

        self.verticalLayout.addWidget(self.treeView)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.filterLine = QLineEdit(self.verticalLayoutWidget)
        self.filterLine.setObjectName(u"filterLine")
        self.filterLine.setEnabled(True)

        self.horizontalLayout_2.addWidget(self.filterLine)

        self.filterButton = QToolButton(self.verticalLayoutWidget)
        self.filterButton.setObjectName(u"filterButton")
        self.filterButton.setEnabled(True)
        icon12 = QIcon(QIcon.fromTheme(u"view-filter"))
        self.filterButton.setIcon(icon12)

        self.horizontalLayout_2.addWidget(self.filterButton)

        self.filterConfig = QToolButton(self.verticalLayoutWidget)
        self.filterConfig.setObjectName(u"filterConfig")
        self.filterConfig.setEnabled(False)
        icon13 = QIcon(QIcon.fromTheme(u"configure"))
        self.filterConfig.setIcon(icon13)

        self.horizontalLayout_2.addWidget(self.filterConfig)

        self.filterHelp = QToolButton(self.verticalLayoutWidget)
        self.filterHelp.setObjectName(u"filterHelp")
        self.filterHelp.setEnabled(False)
        icon14 = QIcon(QIcon.fromTheme(u"help-hint"))
        self.filterHelp.setIcon(icon14)

        self.horizontalLayout_2.addWidget(self.filterHelp)


        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.buttonExpandAll = QPushButton(self.verticalLayoutWidget)
        self.buttonExpandAll.setObjectName(u"buttonExpandAll")
        self.buttonExpandAll.setEnabled(True)

        self.horizontalLayout.addWidget(self.buttonExpandAll)

        self.buttonCollapseAll = QPushButton(self.verticalLayoutWidget)
        self.buttonCollapseAll.setObjectName(u"buttonCollapseAll")
        self.buttonCollapseAll.setEnabled(True)

        self.horizontalLayout.addWidget(self.buttonCollapseAll)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.splitterLeftRight.addWidget(self.verticalLayoutWidget)
        self.splitterTopBottom = QSplitter(self.splitterLeftRight)
        self.splitterTopBottom.setObjectName(u"splitterTopBottom")
        self.splitterTopBottom.setOrientation(Qt.Vertical)
        self.splitterTopBottom.setChildrenCollapsible(False)
        self.verticalLayoutWidget_2 = QWidget(self.splitterTopBottom)
        self.verticalLayoutWidget_2.setObjectName(u"verticalLayoutWidget_2")
        self.verticalLayout_2 = QVBoxLayout(self.verticalLayoutWidget_2)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.frame = QFrame(self.verticalLayoutWidget_2)
        self.frame.setObjectName(u"frame")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.gridLayout_2 = QGridLayout(self.frame)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.currentBlockId = QLabel(self.frame)
        self.currentBlockId.setObjectName(u"currentBlockId")
        sizePolicy3 = QSizePolicy(QSizePolicy.Policy.MinimumExpanding, QSizePolicy.Policy.Preferred)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.currentBlockId.sizePolicy().hasHeightForWidth())
        self.currentBlockId.setSizePolicy(sizePolicy3)
        self.currentBlockId.setFont(font)

        self.gridLayout_2.addWidget(self.currentBlockId, 0, 6, 1, 1)

        self.x_lengthLabel = QLabel(self.frame)
        self.x_lengthLabel.setObjectName(u"x_lengthLabel")
        sizePolicy4 = QSizePolicy(QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Preferred)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(0)
        sizePolicy4.setHeightForWidth(self.x_lengthLabel.sizePolicy().hasHeightForWidth())
        self.x_lengthLabel.setSizePolicy(sizePolicy4)

        self.gridLayout_2.addWidget(self.x_lengthLabel, 1, 5, 1, 1)

        self.currentBlockLength = QLabel(self.frame)
        self.currentBlockLength.setObjectName(u"currentBlockLength")
        sizePolicy3.setHeightForWidth(self.currentBlockLength.sizePolicy().hasHeightForWidth())
        self.currentBlockLength.setSizePolicy(sizePolicy3)
        self.currentBlockLength.setFont(font)

        self.gridLayout_2.addWidget(self.currentBlockLength, 1, 6, 1, 1)

        self.x_localOffsetLabel = QLabel(self.frame)
        self.x_localOffsetLabel.setObjectName(u"x_localOffsetLabel")
        sizePolicy4.setHeightForWidth(self.x_localOffsetLabel.sizePolicy().hasHeightForWidth())
        self.x_localOffsetLabel.setSizePolicy(sizePolicy4)

        self.gridLayout_2.addWidget(self.x_localOffsetLabel, 1, 8, 1, 1)

        self.currentBlockLocalOffset = QLabel(self.frame)
        self.currentBlockLocalOffset.setObjectName(u"currentBlockLocalOffset")
        sizePolicy3.setHeightForWidth(self.currentBlockLocalOffset.sizePolicy().hasHeightForWidth())
        self.currentBlockLocalOffset.setSizePolicy(sizePolicy3)
        self.currentBlockLocalOffset.setFont(font)

        self.gridLayout_2.addWidget(self.currentBlockLocalOffset, 1, 9, 1, 1)

        self.currentBlockType = QLabel(self.frame)
        self.currentBlockType.setObjectName(u"currentBlockType")
        sizePolicy3.setHeightForWidth(self.currentBlockType.sizePolicy().hasHeightForWidth())
        self.currentBlockType.setSizePolicy(sizePolicy3)
        self.currentBlockType.setFont(font)

        self.gridLayout_2.addWidget(self.currentBlockType, 1, 1, 1, 1)

        self.x_titleLabel = QLabel(self.frame)
        self.x_titleLabel.setObjectName(u"x_titleLabel")
        sizePolicy4.setHeightForWidth(self.x_titleLabel.sizePolicy().hasHeightForWidth())
        self.x_titleLabel.setSizePolicy(sizePolicy4)

        self.gridLayout_2.addWidget(self.x_titleLabel, 0, 0, 1, 1)

        self.currentBlockOffset = QLabel(self.frame)
        self.currentBlockOffset.setObjectName(u"currentBlockOffset")
        sizePolicy3.setHeightForWidth(self.currentBlockOffset.sizePolicy().hasHeightForWidth())
        self.currentBlockOffset.setSizePolicy(sizePolicy3)
        self.currentBlockOffset.setFont(font)

        self.gridLayout_2.addWidget(self.currentBlockOffset, 0, 9, 1, 1)

        self.x_divider_1 = QFrame(self.frame)
        self.x_divider_1.setObjectName(u"x_divider_1")
        self.x_divider_1.setFrameShadow(QFrame.Plain)
        self.x_divider_1.setFrameShape(QFrame.VLine)

        self.gridLayout_2.addWidget(self.x_divider_1, 0, 4, 2, 1)

        self.x_offsetLabel = QLabel(self.frame)
        self.x_offsetLabel.setObjectName(u"x_offsetLabel")
        sizePolicy4.setHeightForWidth(self.x_offsetLabel.sizePolicy().hasHeightForWidth())
        self.x_offsetLabel.setSizePolicy(sizePolicy4)

        self.gridLayout_2.addWidget(self.x_offsetLabel, 0, 8, 1, 1)

        self.x_divider_2 = QFrame(self.frame)
        self.x_divider_2.setObjectName(u"x_divider_2")
        self.x_divider_2.setFrameShadow(QFrame.Plain)
        self.x_divider_2.setFrameShape(QFrame.VLine)

        self.gridLayout_2.addWidget(self.x_divider_2, 0, 7, 2, 1)

        self.x_idLabel = QLabel(self.frame)
        self.x_idLabel.setObjectName(u"x_idLabel")
        sizePolicy4.setHeightForWidth(self.x_idLabel.sizePolicy().hasHeightForWidth())
        self.x_idLabel.setSizePolicy(sizePolicy4)

        self.gridLayout_2.addWidget(self.x_idLabel, 0, 5, 1, 1)

        self.currentBlockTitle = QLabel(self.frame)
        self.currentBlockTitle.setObjectName(u"currentBlockTitle")
        sizePolicy3.setHeightForWidth(self.currentBlockTitle.sizePolicy().hasHeightForWidth())
        self.currentBlockTitle.setSizePolicy(sizePolicy3)
        self.currentBlockTitle.setFont(font)

        self.gridLayout_2.addWidget(self.currentBlockTitle, 0, 1, 1, 1)

        self.x_typeLabel = QLabel(self.frame)
        self.x_typeLabel.setObjectName(u"x_typeLabel")
        sizePolicy4.setHeightForWidth(self.x_typeLabel.sizePolicy().hasHeightForWidth())
        self.x_typeLabel.setSizePolicy(sizePolicy4)

        self.gridLayout_2.addWidget(self.x_typeLabel, 1, 0, 1, 1)


        self.verticalLayout_2.addWidget(self.frame)

        self.docsView = QTextBrowser(self.verticalLayoutWidget_2)
        self.docsView.setObjectName(u"docsView")
        sizePolicy5 = QSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Expanding)
        sizePolicy5.setHorizontalStretch(0)
        sizePolicy5.setVerticalStretch(1)
        sizePolicy5.setHeightForWidth(self.docsView.sizePolicy().hasHeightForWidth())
        self.docsView.setSizePolicy(sizePolicy5)

        self.verticalLayout_2.addWidget(self.docsView)

        self.splitterTopBottom.addWidget(self.verticalLayoutWidget_2)
        self.contentsViewScrollArea = QScrollArea(self.splitterTopBottom)
        self.contentsViewScrollArea.setObjectName(u"contentsViewScrollArea")
        sizePolicy5.setHeightForWidth(self.contentsViewScrollArea.sizePolicy().hasHeightForWidth())
        self.contentsViewScrollArea.setSizePolicy(sizePolicy5)
        self.contentsViewScrollArea.setWidgetResizable(True)
        self.contentsViewScrollArea.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignTop)
        self.contentsView = QWidget()
        self.contentsView.setObjectName(u"contentsView")
        self.contentsView.setGeometry(QRect(0, 0, 427, 242))
        self.contentsViewScrollArea.setWidget(self.contentsView)
        self.splitterTopBottom.addWidget(self.contentsViewScrollArea)
        self.splitterLeftRight.addWidget(self.splitterTopBottom)

        self.verticalLayout_3.addWidget(self.splitterLeftRight)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 800, 23))
        self.menuFil = QMenu(self.menubar)
        self.menuFil.setObjectName(u"menuFil")
        self.menuHelp = QMenu(self.menubar)
        self.menuHelp.setObjectName(u"menuHelp")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.menubar.addAction(self.menuFil.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())
        self.menuFil.addAction(self.actionNew)
        self.menuFil.addAction(self.actionQuickOpen)
        self.menuFil.addAction(self.actionOpen)
        self.menuFil.addSeparator()
        self.menuFil.addAction(self.actionSave)
        self.menuFil.addAction(self.actionSaveAs)
        self.menuFil.addSeparator()
        self.menuFil.addAction(self.actionExportXML)
        self.menuFil.addAction(self.actionExportMod)
        self.menuFil.addSeparator()
        self.menuFil.addAction(self.actionQuit)
        self.menuHelp.addAction(self.actionSpecs)
        self.menuHelp.addAction(self.actionDocs)
        self.menuHelp.addSeparator()
        self.menuHelp.addAction(self.actionAbout)
        self.menuHelp.addAction(self.actionAboutQt)

        self.retranslateUi(MainWindow)
        self.actionQuit.triggered.connect(MainWindow.close)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"A\u2082E \u2014 Unnamed [*]", None))
        self.actionDocs.setText(QCoreApplication.translate("MainWindow", u"Documentation", None))
#if QT_CONFIG(shortcut)
        self.actionDocs.setShortcut(QCoreApplication.translate("MainWindow", u"F2", None))
#endif // QT_CONFIG(shortcut)
        self.actionSpecs.setText(QCoreApplication.translate("MainWindow", u"Data file specifications", None))
#if QT_CONFIG(shortcut)
        self.actionSpecs.setShortcut(QCoreApplication.translate("MainWindow", u"F1", None))
#endif // QT_CONFIG(shortcut)
        self.actionAbout.setText(QCoreApplication.translate("MainWindow", u"About A\u2082E", None))
        self.actionQuickOpen.setText(QCoreApplication.translate("MainWindow", u"Quick open", None))
#if QT_CONFIG(shortcut)
        self.actionQuickOpen.setShortcut(QCoreApplication.translate("MainWindow", u"Ctrl+O", None))
#endif // QT_CONFIG(shortcut)
        self.actionQuit.setText(QCoreApplication.translate("MainWindow", u"Quit", None))
#if QT_CONFIG(shortcut)
        self.actionQuit.setShortcut(QCoreApplication.translate("MainWindow", u"Ctrl+Q", None))
#endif // QT_CONFIG(shortcut)
        self.actionSaveAs.setText(QCoreApplication.translate("MainWindow", u"Save as...", None))
#if QT_CONFIG(shortcut)
        self.actionSaveAs.setShortcut(QCoreApplication.translate("MainWindow", u"Ctrl+Shift+S", None))
#endif // QT_CONFIG(shortcut)
        self.actionSave.setText(QCoreApplication.translate("MainWindow", u"Save changes", None))
#if QT_CONFIG(shortcut)
        self.actionSave.setShortcut(QCoreApplication.translate("MainWindow", u"Ctrl+S", None))
#endif // QT_CONFIG(shortcut)
        self.actionExportXML.setText(QCoreApplication.translate("MainWindow", u"Export file as XML", None))
#if QT_CONFIG(shortcut)
        self.actionExportXML.setShortcut(QCoreApplication.translate("MainWindow", u"Ctrl+E", None))
#endif // QT_CONFIG(shortcut)
        self.actionNew.setText(QCoreApplication.translate("MainWindow", u"New", None))
#if QT_CONFIG(shortcut)
        self.actionNew.setShortcut(QCoreApplication.translate("MainWindow", u"Ctrl+N", None))
#endif // QT_CONFIG(shortcut)
        self.actionExportMod.setText(QCoreApplication.translate("MainWindow", u"Export changes as Mod", None))
#if QT_CONFIG(shortcut)
        self.actionExportMod.setShortcut(QCoreApplication.translate("MainWindow", u"Ctrl+M", None))
#endif // QT_CONFIG(shortcut)
        self.actionOpen.setText(QCoreApplication.translate("MainWindow", u"Open", None))
#if QT_CONFIG(shortcut)
        self.actionOpen.setShortcut(QCoreApplication.translate("MainWindow", u"Ctrl+L", None))
#endif // QT_CONFIG(shortcut)
        self.actionAboutQt.setText(QCoreApplication.translate("MainWindow", u"About Qt", None))
        self.x_currentFileLabel.setText(QCoreApplication.translate("MainWindow", u"Current file:", None))
        self.currentFileName.setText(QCoreApplication.translate("MainWindow", u"new unsaved file", None))
        ___qtreewidgetitem = self.treeWidget.headerItem()
        ___qtreewidgetitem.setText(4, QCoreApplication.translate("MainWindow", u"Offset", None));
        ___qtreewidgetitem.setText(3, QCoreApplication.translate("MainWindow", u"Length", None));
        ___qtreewidgetitem.setText(2, QCoreApplication.translate("MainWindow", u"Type", None));
        ___qtreewidgetitem.setText(1, QCoreApplication.translate("MainWindow", u"ID", None));
        ___qtreewidgetitem.setText(0, QCoreApplication.translate("MainWindow", u"Title", None));
        self.filterLine.setPlaceholderText(QCoreApplication.translate("MainWindow", u"p:ANNO/ANLAGEN/ENTRY@101", None))
#if QT_CONFIG(tooltip)
        self.filterButton.setToolTip(QCoreApplication.translate("MainWindow", u"Filter nodes", None))
#endif // QT_CONFIG(tooltip)
        self.filterButton.setText(QCoreApplication.translate("MainWindow", u"Filter", None))
#if QT_CONFIG(shortcut)
        self.filterButton.setShortcut(QCoreApplication.translate("MainWindow", u"F3", None))
#endif // QT_CONFIG(shortcut)
#if QT_CONFIG(tooltip)
        self.filterConfig.setToolTip(QCoreApplication.translate("MainWindow", u"Configure how to filter nodes", None))
#endif // QT_CONFIG(tooltip)
        self.filterConfig.setText(QCoreApplication.translate("MainWindow", u"Configure", None))
#if QT_CONFIG(shortcut)
        self.filterConfig.setShortcut(QCoreApplication.translate("MainWindow", u"Ctrl+X", None))
#endif // QT_CONFIG(shortcut)
        self.filterHelp.setText(QCoreApplication.translate("MainWindow", u"Show help", None))
        self.buttonExpandAll.setText(QCoreApplication.translate("MainWindow", u"Expand all", None))
#if QT_CONFIG(shortcut)
        self.buttonExpandAll.setShortcut(QCoreApplication.translate("MainWindow", u"Ctrl+A", None))
#endif // QT_CONFIG(shortcut)
        self.buttonCollapseAll.setText(QCoreApplication.translate("MainWindow", u"Collapse all", None))
        self.currentBlockId.setText(QCoreApplication.translate("MainWindow", u"(none)", None))
        self.x_lengthLabel.setText(QCoreApplication.translate("MainWindow", u"Length:", None))
        self.currentBlockLength.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.x_localOffsetLabel.setText(QCoreApplication.translate("MainWindow", u"Local offset:", None))
        self.currentBlockLocalOffset.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.currentBlockType.setText(QCoreApplication.translate("MainWindow", u"none", None))
        self.x_titleLabel.setText(QCoreApplication.translate("MainWindow", u"Title:", None))
        self.currentBlockOffset.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.x_offsetLabel.setText(QCoreApplication.translate("MainWindow", u"Offset:", None))
        self.x_idLabel.setText(QCoreApplication.translate("MainWindow", u"Identifier:", None))
        self.currentBlockTitle.setText(QCoreApplication.translate("MainWindow", u"none", None))
        self.x_typeLabel.setText(QCoreApplication.translate("MainWindow", u"Type:", None))
        self.menuFil.setTitle(QCoreApplication.translate("MainWindow", u"File", None))
        self.menuHelp.setTitle(QCoreApplication.translate("MainWindow", u"Help", None))
    # retranslateUi

